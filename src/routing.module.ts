import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { GuitarsDetailPageComponent } from "./app/guitars-detail-page/guitars-detail-page.component";
import { GuitarsPageComponent } from "./app/guitars-page/guitars-page.component";
import { LoginPageComponent } from "./app/login-page/login-page.component";

const routes: Routes=[
    {
        path:'',
        component:LoginPageComponent
    },
    {
        path:'guitars',
        component:GuitarsPageComponent
    },
    {
        path:'guitarDetails',
        component:GuitarsDetailPageComponent
    }
]

@NgModule({
    imports:[RouterModule.forRoot(routes)],
    exports:[RouterModule]
})
export class AppRoutingModule{}