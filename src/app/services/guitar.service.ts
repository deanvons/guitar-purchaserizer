import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {Guitar} from '../models/guitar'
// this service is responsible for fetching the data (using http client)
// this is true for services in angular (fetching data should happen in services)
@Injectable({
  providedIn: 'root'
})
// the injectable decorator means this class (object) can be shared across components (using dependancy injection) -> we will inject into the guitar-list.component.ts to get access to the function that fetches data getGuitars()
export class GuitarService {

  private guitarURL: string = "https://juniper-western-direction.glitch.me/guitars"
  private selectedGuitar?:Guitar

  // because this class is shared across components we can use it for lifted state (like redux)
  // properties set here are used as lifted state

  constructor(private readonly http:HttpClient) { }

  // this function returns an observable so that the component who invokes it can access the result
  getGuitars(): Observable<Guitar[]> {
    // in angular we don't use promises instead we use a construct called an Observable
    // it does the same thing, waits for some operation and then does something with it
    // instead of .then(response=>response.json()).then(result=> // use result)
    // we use .subscribe(result=> // use result) it automatically extracts the json payload for us
    // if we want to change the response we can use the pipe feature more on this later :)
      return this.http.get<Guitar[]>(this.guitarURL)
    }

    getGuitarById(guitarId:string){
      return this.http.get<Guitar>(`${this.guitarURL}/${guitarId}`)
    }

    set setSelectedGuitar(value:Guitar){
      this.selectedGuitar = value
    }

    get guitarSelected(){
      return this.selectedGuitar
    }


}
