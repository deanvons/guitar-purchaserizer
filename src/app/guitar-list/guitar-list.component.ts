import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { getGuitar } from 'src/Utils/api';
import { Guitar } from '../models/guitar';
import { GuitarService } from '../services/guitar.service';

@Component({
  selector: 'app-guitar-list',
  templateUrl: './guitar-list.component.html',
  styleUrls: ['./guitar-list.component.css']
})
export class GuitarListComponent implements OnInit {

  // we can inject any service into any component using the constructor
  // make sure it is private and readonly
  constructor(private readonly guitarService: GuitarService, private readonly router: Router) { }

  guitarData?: Guitar[]


  ngOnInit(): void {
    // change the mock data below to use the service instead
    // this.guitarData = getGuitar()
    this.guitarService.getGuitars()
      .subscribe({
        // this is where we use the data
        next: result => this.guitarData = result,
        // this is where we catch errors
        error: error => console.log(error),
        // this where we can put final code if we want
        complete: () => {
          console.log("It's done")
        }
      })
  }

  handleGuitarSelected(guitarId: string) {

    this.router.navigate(['guitarDetails'])

    let guitarResult = this.guitarData?.filter(guitar => guitar.id === guitarId)

    if (guitarResult !== undefined) {

      this.guitarService.setSelectedGuitar = guitarResult[0]
    }


  }
}