import { Component, OnInit,Input } from '@angular/core';
import { Guitar } from "src/app/models/guitar";
import { GuitarService } from '../services/guitar.service';

@Component({
  selector: 'app-guitar-detail',
  templateUrl: './guitar-detail.component.html',
  styleUrls: ['./guitar-detail.component.css']
})
export class GuitarDetailComponent implements OnInit {

  

  constructor(private readonly guitarService:GuitarService) { }

  ngOnInit(): void {
  }

  get guitar():Guitar|undefined{
    return this.guitarService.guitarSelected
  }

}
