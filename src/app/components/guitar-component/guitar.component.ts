import { Component, Input, Output, EventEmitter } from "@angular/core";
import { Guitar } from "src/app/models/guitar";
@Component(
    {
        selector: 'app-guitar',
        templateUrl: './guitar.component.html',
        styleUrls: ['./guitar.component.css']
    }
)
export class GuitarComponent {

    @Input() guitarParam?: Guitar
    @Output() guitarSelected: EventEmitter<any> = new EventEmitter()

    handleGuitarSelected() {
        this.guitarSelected.emit(this.guitarParam?.id)
    }

}