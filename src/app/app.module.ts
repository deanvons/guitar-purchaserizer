import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http'

import { AppComponent } from './app.component';
import { GuitarComponent } from './components/guitar-component/guitar.component';
import { GuitarListComponent } from './guitar-list/guitar-list.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { GuitarsPageComponent } from './guitars-page/guitars-page.component';
import { GuitarsDetailPageComponent } from './guitars-detail-page/guitars-detail-page.component';
import { AppRoutingModule } from 'src/routing.module';
import { GuitarDetailComponent } from './guitar-detail/guitar-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    GuitarComponent,
    GuitarListComponent,
    LoginPageComponent,
    GuitarsPageComponent,
    GuitarsDetailPageComponent,
    GuitarDetailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }