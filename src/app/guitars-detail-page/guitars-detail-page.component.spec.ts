import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GuitarsDetailPageComponent } from './guitars-detail-page.component';

describe('GuitarsDetailPageComponent', () => {
  let component: GuitarsDetailPageComponent;
  let fixture: ComponentFixture<GuitarsDetailPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GuitarsDetailPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GuitarsDetailPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
